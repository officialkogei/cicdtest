package com.starkogi.cicddemo.entry;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/cicd-services/v1/")
public class Controllers {

    @GetMapping("/get")
    private ResponseEntity<Object> contentBundlingCallback(@RequestParam String input){

        System.out.println(input);

        return new ResponseEntity<>(input, HttpStatus.OK);
    }
}
